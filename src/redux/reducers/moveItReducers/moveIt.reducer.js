import {
  RESET_TOTAL,
  ADD_MOVING_SUBJECT,
  SET_TOTAL_VALUE,
} from '../../constants/constants'

const initState = {
  formFields: ['id', 'total', 'fullName', 'mail', 'movingFrom', 'movingTo', 'distance','livingArea', 'atticArea', 'isPiano'],
  movingSubjects: [],
  totalValue: 0,
}

const moveItReducer = (state = initState, action) => {
  switch (action.type) {
    case RESET_TOTAL:
      return { ...state, totalValue: 0 };
    case ADD_MOVING_SUBJECT:
      return { ...state, movingSubjects: [...state.movingSubjects, action.payload] };
    case SET_TOTAL_VALUE:
      return { ...state, totalValue: action.payload };
    default: return state
  }
}
export default moveItReducer
