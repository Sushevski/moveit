import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import moveItReducer from '../reducers/moveItReducers/moveIt.reducer';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'persisted-values',
  // blacklist: ['moveItStore'], //comment line to turn on persistor
  storage,
}

const rootReducer = combineReducers({
  moveItStore: moveItReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer)
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(persistedReducer, composeEnhancers(applyMiddleware()));

const Persistor = persistStore(store)
export default store;
export {Persistor}
