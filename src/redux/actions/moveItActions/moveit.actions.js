import {
  RESET_TOTAL,
  ADD_MOVING_SUBJECT,
  SET_TOTAL_VALUE,
} from '../../constants/constants'

export const addMovingSubject = (movingSubject) => {
  return {
    type: ADD_MOVING_SUBJECT,
    payload: movingSubject
  }
}

export const setTotalValue = (totalValue) => {
  return {
    type: SET_TOTAL_VALUE,
    payload: totalValue
  }
}

export const resetTotalValReducer = () => {
  return {
    type: RESET_TOTAL
  }
}