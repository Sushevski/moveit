import React from 'react'
import { connect } from 'react-redux';
import { resetTotalValReducer } from '../../redux/actions/moveItActions/moveit.actions';
import { dynamicId } from '../../utils/dynamicId';
import labelsExport from '../../utils/labelsExport';
class Listing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
    this.props.resetTotalValReducer()
  }

  render() {
    let { movingSubjects } = this.props
    return (
      <React.Fragment>
        {movingSubjects.length > 0 ?
          movingSubjects.map((subj, i) => {
            let titles = [], infos = []
            for (const property in subj) {
              titles.push(<th key={i + '_title' + dynamicId()} scope="col">{labelsExport(property)}</th>)
              infos.push(<td key={i + '_info' + dynamicId()}>{subj[property]}</td>)
            }
            return <div key={i + '_main' + dynamicId()} className="card no-zoom">
              <h5 className="card-header">Request number: {subj.id}</h5>
              <div className="card-body">
                <table className="table table-striped table-dark">
                  <thead>
                    <tr>
                      {titles}
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      {infos}
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          })
          : <h1>There are no moving subjects listed</h1>
        }
      </React.Fragment>
    );
  }
}


const mapStateToProps = (state) => ({
  movingSubjects: state.moveItStore.movingSubjects
})

const mapDispatchToProps = (dispatch) => ({
  resetTotalValReducer: () => dispatch(resetTotalValReducer())
})

export default connect(mapStateToProps, mapDispatchToProps)(Listing)

