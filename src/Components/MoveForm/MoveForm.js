import React from 'react'
import { connect } from 'react-redux';
import { Formik, Form } from 'formik';
import { Input } from '../InputComponent/Input';
import { Select } from '../InputComponent/Select'
import { formValidation } from '../../utils/formValidation';
import { addMovingSubject, setTotalValue, resetTotalValReducer } from '../../redux/actions/moveItActions/moveit.actions';
import { calculateTotal } from '../../utils/calculateTotal';
import labelsExport from '../../utils/labelsExport';
import { dynamicId } from '../../utils/dynamicId';
import Swal from 'sweetalert2';

const MoveForm = ({ formFields, addMovingSubject, totalValue, setTotalValue, resetTotalValReducer }) => {
  const initValsTmp = {}
  for (const key of formFields) {
    if (key === 'isPiano') {
      initValsTmp[key] = 'no'
    } else {
      if (key === 'distance' || key === 'livingArea' || key === 'atticArea') {
        initValsTmp[key] = 0
      } else {
        initValsTmp[key] = ''
      }
    }
  }

  const doCalculation = (values) => {
    let total = calculateTotal(values)
    if (total) {
      setTotalValue(total)
    }
  }

  return (
    <React.Fragment>
      <Formik
        initialValues={
          initValsTmp
        }
        onSubmit={(values, { resetForm, setSubmitting  }) => {
          setSubmitting(false)
          let tmpValTotal
          if (totalValue && totalValue !== 0) {
            tmpValTotal = totalValue
          } else {
            tmpValTotal = values.total
          }
          if(tmpValTotal) {
            let tmpId = dynamicId()
            values.id = tmpId
            setSubmitting(true)
            values.total = tmpValTotal
            addMovingSubject(values)
            resetForm({ values: '' })
            resetTotalValReducer()
            Swal.fire({
              icon: 'success',
              title: 'We saved your request successfully',
              text: 'You can check your data in the listings',
            })
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Calculate the price first :D !',
            })
          }
        }}
        validate={(values) => formValidation(values)}
      >
        {formik => (
          <div className='container'>
            <h3>Add Moving Subject</h3>
            <Form>
              {formFields.map((field, id) => {
                if (field !== 'isPiano') {
                  if (field === 'total') {
                    let value
                    if (field === 'total') {
                      value = totalValue
                    }
                    return <p key={field + id}>
                      {labelsExport(field)} : {value}
                      {formik.errors[field] && formik.touched[field] && <div className='text-warning' key={'error' + field}>{formik.errors[field]}</div>}
                    </p>
                  } else if (field !== 'isPiano' && field !== 'id' && field !== 'total') {
                    return <React.Fragment key={field + '_input'}>
                      <Input  label={field} name={field} type={'text'} formik={formik} />
                      {formik.errors[field] && formik.touched[field] && <div className='text-warning' key={'error' + field}>{formik.errors[field]}</div>}
                    </React.Fragment>
                  }
                } else {
                  return <React.Fragment key={field + '_select'}>
                    <Select label={field} name={field} formik={formik} />
                    {formik.errors[field] && formik.touched[field] && <div className='text-warning' key={'error' + field}>{formik.errors[field]}</div>}
                  </React.Fragment>
                }
              })}
              <div className='buttons-flex'>
                <button className="btn btn-info btn-sm" onClick={doCalculation.bind(null, formik.values)} type="button">Calculate Price</button>
                <button type="submit" className='btn btn-success btn-sm'>Add Subject</button>
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </React.Fragment>
  );
}

const mapStateToProps = (state) => ({
  formFields: state.moveItStore.formFields,
  totalValue: state.moveItStore.totalValue,
})

const mapDispatchToProps = (dispatch) => ({
  addMovingSubject: (movingSubject) => dispatch(addMovingSubject(movingSubject)),
  setTotalValue: (totalVal) => dispatch(setTotalValue(totalVal)),
  resetTotalValReducer: () => dispatch(resetTotalValReducer())
})

export default connect(mapStateToProps, mapDispatchToProps)(MoveForm)