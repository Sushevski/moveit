import { Link } from 'react-router-dom';
import { home, login } from '../../utils/svg'

export default function Navigation() {
  return (
    <nav className="navbar navbar-dark bg-dark ">
      <Link to="/" className="navbar-brand">
        <div className='flex'>
          <div className='logo'>{home}</div>
          <div className='logo-desc'>Move it</div>
        </div>
      </Link>
      <div className='logo'>{login}</div>
    </nav>
  );
}
