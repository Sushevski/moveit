import React from "react";
import { Field } from 'formik';
import labelsExport from "../../utils/labelsExport";

export const Select = (props) => {
  let options = [
    'no', 'yes'
  ]
  return (
    <div className="form-group row">
      <label htmlFor={props.label} className="col-sm-2 col-form-label col-form-label-sm">{labelsExport(props.label)}</label>
      <div className="col-sm-10">
        <Field as='select' className="form-control" id={props.label} name={props.label} onBlur={props.formik.handleBlur}>
          <option key={'default_' + props.label} value='' disabled>Select Value</option>
          {
            options.map((option) => {
              return <option key={option} value={option}>{option}</option>
            })
          }
        </Field>
      </div>
    </div>
  )
}