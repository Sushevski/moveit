import React from "react";
import { Field } from 'formik';
import labelsExport from "../../utils/labelsExport";

export const Input = ({ label, ...props }) => {
  return (
    <div className="form-group row">
      <label htmlFor={label} className="col-sm-2 col-form-label col-form-label-sm">{labelsExport(label)}</label>
      <div className="col-sm-10">
        <Field className='form-control form-control-sm' disabled={props.disabled} type={label === 'livingArea' || label === 'atticArea' || label === 'distance' ? 'number' : 'text'} name={label} onBlur={props.formik.handleBlur} />
      </div>
    </div>
  )
}
