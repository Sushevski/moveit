import { Link } from "react-router-dom";

export default function MainRoute() {
  return (
    <nav className="custom-nav">
      <Link to='/listing' className="custom-link">
        <div className="card" style={{width: '18rem'}}>
          <img src="https://images.pexels.com/photos/196650/pexels-photo-196650.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" className="card-img-top" alt="listings"/>
            <div className="card-body">
              <p className="card-text">List all your moving subjects</p>
            </div>
        </div>
      </Link>
      <Link to='/move' className="custom-link">
        <div className="card" style={{width: '18rem'}}>
          <img src="https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" className="card-img-top" alt="move-subject"/>
            <div className="card-body">
              <p className="card-text">Request moving to your subjects</p>
            </div>
        </div>
      </Link>
    </nav>
  );
}
