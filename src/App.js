import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";
import Listing from './Components/Listing/Listing'
import MoveForm from './Components/MoveForm/MoveForm'
import Navigation from './Components/Navigation/Navigation'
import MainRoute from './Components/MainRoute/MainRoute';

function App() {
  return (
    <div className="App">
      <Router>
        <Navigation />
        <Routes>
          <Route exact path='/' element={<MainRoute />} />
          <Route exact path='/listing' element={<Listing />} />
          <Route exact path='/move' element={<MoveForm />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;