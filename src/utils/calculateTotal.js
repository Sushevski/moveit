export const calculateTotal = ({ distance, livingArea, atticArea, isPiano }) => {
  let totalDistancePrice, totalNumberOfCars, totalCarsCost
  if (isPiano === 'yes') {
    return 5000
  } else {
    if (distance || distance === 0) {
      if (distance => 1 && distance <= 10) {
        totalDistancePrice = 1100
      } else if (distance > 10 && distance <= 49) {
        totalDistancePrice = 1000 + parseInt(distance * 10)
      } else if (distance === 50) {
        totalDistancePrice = 5400
      } else if (distance > 50 && distance <= 99) {
        totalDistancePrice = 5000 + parseInt(distance * 8)
      } else if (distance === 100) {
        totalDistancePrice = 10700
      } else if (distance > 100) {
        totalDistancePrice = 10000 + parseInt(distance * 7)
      }
    }
    if (livingArea && !!!atticArea) {
      if (livingArea <= 49) {
        totalNumberOfCars = 1
      } else {
        totalNumberOfCars = Math.trunc(livingArea / 50 + 1)
      }
      totalCarsCost = 1100 * totalNumberOfCars
    }
    if (atticArea && !!!livingArea) {
      let tmpVolumeAttic = parseInt(atticArea * 2)
      totalNumberOfCars = Math.trunc(tmpVolumeAttic / 50 + 1)
      totalCarsCost = 1100 * totalNumberOfCars
    }
    if (livingArea && atticArea) {
      let tmpVolumeAttic = parseInt(atticArea * 2)
      let tmpVolumeTotal = tmpVolumeAttic + livingArea
      totalNumberOfCars = Math.trunc(tmpVolumeTotal / 50 + 1)
      totalCarsCost = 1100 * totalNumberOfCars
    }
    return totalDistancePrice + totalCarsCost
  }
}
