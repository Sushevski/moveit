export const formValidation = (values) => {
  const errors = {}
  if (!values.fullName) {
    errors.fullName = 'Requred field'
  }
  if (!values.mail) {
    errors.mail = 'Requred field'
  } else if (values.mail) {
    const emailRegex = /\S+@\S+\.\S+/;
    if (!(emailRegex.test(values.mail))) {
      errors.mail = 'Please enter a valid mail'
    }
  }
  if (!values.movingFrom) {
    errors.movingFrom = 'Requred field'
  }
  if (!values.movingTo) {
    errors.movingTo = 'Requred field'
  }
  if ((!values.distance || values.distance <= 0) && values.isPiano === 'no') {
    errors.distance = 'Distance must be at least 1 km'
  }
  if ((!values.atticArea || values.atticArea === 0) && (!values.livingArea || values.livingArea === 0) && values.isPiano === 'no') {
    errors.atticArea = 'Attic area must be at least 1 m2, or enter living area'
    errors.livingArea = 'Living area must be at least 1 m2, or enter attic area'
  }
  return errors
}
