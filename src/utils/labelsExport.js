export default function labelsExport(value) {
  let label
  switch (value) {
    case 'fullName':
      label = 'Full name'
      return label;
    case 'mail':
      label = 'Mail'
      return label;
    case 'movingFrom':
      label = 'Moving from'
      return label;
    case 'movingTo':
      label = 'Moving to'
      return label;
    case 'distance':
      label = 'Distance /km'
      return label;
    case 'livingArea':
      label = 'Living area /m2'
      return label;
    case 'atticArea':
      label = 'Attic area /m2'
      return label;
    case 'isPiano':
      label = 'Is a piano?'
      return label;
    case 'id':
      label = 'Generated id'
      return label;
    case 'total':
      label = 'Total Price in SEK'
      return label;
    default:
      return label;
  }
}